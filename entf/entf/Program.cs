﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
//Jacobo Jacobo Miguel Anguel
//Mejia Martinez Jose Manuel

namespace entf
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                //new Program().archivos("Prueba1.txt");
                if (args.Any())
                {
                    System.Console.WriteLine("Archivo Fuente u Origen " + args[0]);
                    Console.WriteLine();
                    new Program().archivos( args[0] );
                }
                else
                {
                    Console.WriteLine("Sin parámetros");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
        }

        private void archivos(string name)
        {
            StreamReader reader = File.OpenText(name);
            string[] arrTemp = name.Split('.');
            StreamWriter fileB = File.CreateText(arrTemp[0] + ".usw");

            System.Console.WriteLine("Archivo Destino o Final " + arrTemp[0] + ".usw");

            string cadena = reader.ReadLine();

            while (cadena != null)
            {
                while (cadena.Equals(string.Empty))
                {
                    cadena = reader.ReadLine();
                    if (cadena == null)
                    {
                        break;
                    }
                }

                if (cadena == null)
                {
                    break;
                }                

                for (int i = 0; i < cadena.Length; i++)
                {
                    cadena = cadena.Replace("  "," ");
                    cadena = cadena.Trim();
                    cadena = cadena.Replace("\t", string.Empty);

                    if (cadena.Equals(string.Empty))
                    {
                        cadena = reader.ReadLine();
                        break;
                    }

                    if (cadena[i] == '/' && cadena[i + 1] == '/')
                    {
                        if (i > 1)
                        {
                            string temp = "";
                            for (int j = 0; j < i; j++)
                            {
                                temp += cadena[j];
                            }
                            cadena = temp;
                            break;
                        }
                        else
                        {
                            cadena = reader.ReadLine();
                        }

                        i = 0;
                        if (cadena == null)
                        {
                            break;
                        }
                    }
                }

                fileB.WriteLine(cadena);
                cadena = reader.ReadLine();
            }
            fileB.Close();
            reader.Close();
        }
    }
}
